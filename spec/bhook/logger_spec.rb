# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Logging' do
  let(:message) { 'foo' }
  let(:formatted_entry) { Bhook::LOG_FORMATTER.call(nil, nil, nil, message) }

  it 'log formatter includes thread id' do
    thread_id = Thread.current.object_id.to_s
    expect(formatted_entry).to include(thread_id)
  end

  it 'log formatter includes message' do
    expect(formatted_entry).to include(message)
  end
end

# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::ThemeGenerator do
  let(:theme_path) { out_path.join('theme').to_s }
  let(:theme_files) do
    [Bhook::PAGE_TEMPLATE_PATH, Bhook::AFTER_H1_TEMPLATE_PATH]
  end

  it 'copies over files to the specified dir' do
    expect(FileUtils).to receive(:mkdir_p).with(theme_path)
    expect(FileUtils).to receive(:cp).with(theme_files, theme_path)
    described_class.new(out_path.to_s).generate!
  end
end

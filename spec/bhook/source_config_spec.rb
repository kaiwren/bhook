# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Bhook::Config' do
  let(:fixture_config) { Bhook::SourceConfig.new(src_path) }

  it 'does not exclude files not in the exclude list' do
    expect(fixture_config.excluded?(Pathname.new('foo.md'))).to be(false)
  end

  it 'excludes files in the exclude list' do
    expect(fixture_config.excluded?(src_path.join('README.md'))).to be(true)
  end

  context 'with invalid file structure' do
    context 'when excludes has blank entries' do
      let(:path) { build_fixture_path('source_config', 'blank_exclude') }
      let(:invalid_config) { Bhook::SourceConfig.new(path) }

      it 'does not blow up' do
        expect { invalid_config }.not_to raise_error
      end

      it 'does not have the base path as an entry because blank excludes were processed' do
        expect(invalid_config.excluded?(path)).to be(false)
      end

      it 'still has the valid exclude as an entry' do
        expect(invalid_config.excluded?(path.join('README.md'))).to be(true)
      end
    end

    context 'when website has blank entries' do
      let(:path) { build_fixture_path('source_config', 'blank_website') }
      let(:invalid_config) { Bhook::SourceConfig.new(path) }

      it 'does not blow up' do
        expect { invalid_config }.not_to raise_error
      end

      it 'does not have the base path as an entry' do
        file_path = Pathname.new('foo.html')
        expect(invalid_config.website_url_for(file_path, 'abcd123')).to be_nil
      end
    end
  end
end

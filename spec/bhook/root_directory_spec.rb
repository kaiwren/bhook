# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::RootDirectory do
  let(:expected_md_file_paths) do
    %w[/org/product-org.md /index.md /recruiting/operations.md
       /recruiting/interviews.md /recruiting/sourcing.md
       /recruiting/team.md /recruiting/market.md].to_set
  end

  it 'knows all md files in its tree' do
    dir = described_class.new(Pathname.new(src_path), Pathname.new(out_path))
    actual_file_paths = dir.all_md_files.map do |file|
      file.src_file_path.to_s.sub(/.*#{bhook_test_fixture_dirname}/, '')
    end.to_set

    expect(actual_file_paths).to eq(expected_md_file_paths)
  end
end

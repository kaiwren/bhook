# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::ArgsParser do
  def expect_error_missing_args_see_help(argv)
    args_parser = Bhook::ArgsParser.new(argv)

    expect(args_parser).to receive(:puts).with("\nError! missing argument: See --help.\n")
    expect(args_parser.parse).to be_nil
  end

  def expect_error_message_missing_args_see_help(argv)
    args_parser = Bhook::ArgsParser.new(argv)

    expect(args_parser).to receive(:puts).with("\nError! missing argument: #{argv.first}\n")
    expect(args_parser.parse).to be_nil
  end

  it 'generates a std error when -s is missing' do
    expect_error_missing_args_see_help(%w[-o /tmp/out])
  end

  it 'generates a std error when -o is missing' do
    expect_error_missing_args_see_help(%w[-s /tmp/src])
  end

  it 'generates a std error when -s -o and --generate-theme are missing' do
    expect_error_missing_args_see_help(%w[-w -t /tmp/theme])
  end

  it 'generates an error specifying the bad option when -s is missing args' do
    expect_error_message_missing_args_see_help(%w[-s])
  end

  it 'generates an error specifying the bad option when -o is missing args' do
    expect_error_message_missing_args_see_help(%w[-o])
  end

  it 'parses -h just fine' do
    args_parser = described_class.new(%w[-h])
    expect(args_parser.parse.help).to be(true)
  end
end

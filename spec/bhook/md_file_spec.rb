# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::MdFile do
  let(:index_md_filename) { 'index.md' }
  let(:index_md_src_path) { src_path.join(index_md_filename) }
  let(:index_md_out_path) { out_path.join('index.html') }
  let(:index_md_file) { described_class.new(index_md_src_path, out_path, git, blank_config) }
  let(:index_md_sha) { '23f4e978f88f7fb19ed9a60c08082c008fe65757' }

  it 'computes its output path from the src path and the filename' do
    expect(index_md_file.out_file_path).to eq(index_md_out_path)
  end

  it 'knows the date it was last committed with changes' do
    expect(index_md_file.src_file_date).to eq('2022-01-29')
  end

  it 'knows the sha of the last commit with changes' do
    expect(index_md_file.src_file_sha).to eq(index_md_sha)
  end

  it 'src url is nil if no website is set in config' do
    expect(index_md_file.src_file_url).to be_nil
  end

  it 'computes src url if a website is set in config' do
    website_url = 'https://gitlab.com/foo/bar/-/blob/'
    config = Bhook::SourceConfig.new(src_path, { 'website' => website_url })
    md_file = described_class.new(index_md_src_path, out_path, git, config)
    expect(md_file.src_file_url).to eq("#{website_url}#{index_md_sha}/#{index_md_filename}")
  end

  it 'writes itself to the correct file' do
    expect(File).to receive(:write).with(index_md_out_path, kind_of(String))
    index_md_file.write!(theme)
  end
end

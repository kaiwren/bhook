# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::Workspace do
  let(:valid_workspace) { described_class.new(src_path.to_s, out_path.to_s, Bhook::THEME_DIR_PATH) }

  before { clean_fixture_output_dir! }

  it 'generates the expected number of html files when run against the fixture' do
    valid_workspace.process!
    readme_file_count = 1
    expected_file_count = all_md_files_in_src_dir.size - readme_file_count
    expect(all_html_files_in_out_dir.size).to eq(expected_file_count)
  end

  it 'runs process! once before handing off to Listen for filesystem watching' do
    setup_listen_library_mocks_and_expectations
    expect(valid_workspace).to receive(:process!).once
    valid_workspace.watch!
  end

  def setup_listen_library_mocks_and_expectations
    mock_listener = instance_double(Listen::Listener)
    expect(mock_listener).to receive(:start).once
    expect(Listen).to receive(:to).once.and_return(mock_listener)
  end
end

# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::Theme do
  it 'renders markdown to html' do
    index_md_sha = '23f4e978f88f7fb19ed9a60c08082c008fe65757'
    setup_stubs
    theme = described_class.new(Bhook::THEME_DIR_PATH)

    page = theme.render_page(md_file_content, index_md_sha, nil, nil)
    expect(page).to eq(expected_html)
  end

  def setup_stubs
    theme_path = Bhook::THEME_DIR_PATH
    page_erb_path = File.join(theme_path, 'page.erb')
    after_h1_erb_path = File.join(theme_path, '_after_h1.erb')

    allow(File).to receive(:read).with(page_erb_path).and_return(page_template)
    allow(File).to receive(:read).with(after_h1_erb_path).and_return(after_h1_template)
  end

  def expected_html
    <<~EOHTML
      Hello
      <h1 id="hello">Hello</h1>
      subheading

      23f4e978f88f7fb19ed9a60c08082c008fe65757
    EOHTML
  end

  def page_template
    <<~EOPAGE
      <%= src_title %>
      <%= output %>
      <%= src_file_sha %>
      <%= file_url -%>
    EOPAGE
  end

  def after_h1_template
    <<~EOAFTERH1
      subheading
    EOAFTERH1
  end

  def md_file_content
    <<~EOMD
      # Hello
    EOMD
  end
end

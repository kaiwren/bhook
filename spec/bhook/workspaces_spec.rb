# typed: false
# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Bhook::Workspaces do
  let(:valid_workspace) { Bhook::Workspace.new(src_path.to_s, out_path.to_s, Bhook::THEME_DIR_PATH) }
  let(:workspaces) { described_class.new }

  before { clean_fixture_output_dir! }

  it 'generates the expected number of html files when run against the fixture' do
    workspaces.mount(valid_workspace)
    valid_workspace.process!
    readme_file_count = 1
    expected_file_count = all_md_files_in_src_dir.size - readme_file_count
    expect(all_html_files_in_out_dir.size).to eq(expected_file_count)
  end

  it 'runs process! once before handing off to Listen for filesystem watching' do
    setup_listen_library_mocks_and_expectations
    workspaces.mount(valid_workspace)
    expect(valid_workspace).to receive(:process!).once
    workspaces.watch!
  end

  def setup_listen_library_mocks_and_expectations
    mock_listener = instance_double(Listen::Listener)
    expect(mock_listener).to receive(:start).once
    expect(Listen).to receive(:to).once.and_return(mock_listener)
    expect(Kernel).to receive(:sleep).once
  end
end

# typed: false
# frozen_string_literal: true

require 'simplecov'
require 'simplecov-cobertura'

formatters = [SimpleCov::Formatter::CoberturaFormatter, SimpleCov::Formatter::HTMLFormatter]
SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new(formatters)
SimpleCov.profiles.define 'bhook' do
  add_filter do |src_file|
    File.dirname(src_file.filename).include?('spec/bhook')
  end
end
SimpleCov.start 'bhook'

require 'bhook'

module SpecHelpers
  SPEC_HELPER_PATH = __FILE__
  SPEC_HELPER_DIR = Pathname.new(File.dirname(SPEC_HELPER_PATH))
  FIXTURE_PATH = SPEC_HELPER_DIR.join('fixtures')
  GIT_FIXTURE_DIR_NAME = 'bhook-test-fixture'
  GIT_FIXTURE_SRC_PATH = SPEC_HELPER_DIR.join(GIT_FIXTURE_DIR_NAME)
  GIT_FIXTURE_OUT_PATH = SpecHelpers::SPEC_HELPER_DIR.join(FIXTURE_PATH.join('output'))

  def build_fixture_path(*args)
    FIXTURE_PATH.join(File.join(*args))
  end

  def clean_fixture_output_dir!
    FileUtils.rm_rf(GIT_FIXTURE_OUT_PATH)
    FileUtils.mkdir(GIT_FIXTURE_OUT_PATH)
  end

  def all_md_files_in_src_dir
    Dir[File.join(GIT_FIXTURE_SRC_PATH, '**', '*.md')]
  end

  def all_html_files_in_out_dir
    Dir[File.join(GIT_FIXTURE_OUT_PATH, '**', '*.html')]
  end

  def all_files_in_output_dir
    Dir[File.join(GIT_FIXTURE_OUT_PATH, '**', '**')]
  end
end

Bhook::L.level = Logger::WARN

RSpec.shared_context 'with config data' do
  let(:bhook_test_fixture_dirname) { SpecHelpers::GIT_FIXTURE_DIR_NAME }
  let(:src_path) { SpecHelpers::GIT_FIXTURE_SRC_PATH }
  let(:out_path) { Pathname.new(SpecHelpers::GIT_FIXTURE_OUT_PATH) }
end

RSpec.shared_context 'with fixture data' do
  let(:git) { Git.open(src_path) }
  let(:blank_config) { Bhook::SourceConfig.new(src_path) }
  let(:theme) { Bhook::Theme.new(Bhook::THEME_DIR_PATH) }
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.include SpecHelpers
  config.include_context 'with config data'
  config.include_context 'with fixture data'
end

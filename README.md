# Bhook

[![Build Status](https://gitlab.com/kaiwren/bhook/badges/main/pipeline.svg)](https://gitlab.com/kaiwren/bhook/commits/main)
[![Coverage Report](https://gitlab.com/kaiwren/bhook/badges/main/coverage.svg)](https://gitlab.com/kaiwren/bhook/commits/main)
[![Gem Version](https://badge.fury.io/rb/bhook.svg)](https://rubygems.org/gems/bhook)
[![Downloads](http://ruby-gem-downloads-badge.herokuapp.com/bhook?type=total)](https://rubygems.org/gems/bhook)



Bhook is a static website generator that works off a git repo containing a markdown file based wiki, like for example, the [India Startup Wiki](https://gitlab.com/india-startups/wiki).

Since you control the source markdown, Bhook applies no restrictions on what you can put in there. Wanna embed a YouTube video in an `iframe`? Sure thing!

I use this to create [HTML versions of the wiki](https://sidu.in/wiki/startups) and of [my essays](https://sidu.in/essays/) that I can publish.

## Installation

`bhook` requires [`ruby`](https://www.ruby-lang.org/en/documentation/installation/). If you are on OSX or Linux, it is likely to already be installed. 

To install `bhook` run:

    $ gem install bhook

## Usage

Getting help:

	$ bhook --help
	Bhook version 0.3.0
	Usage: bhook --source /source/path --output /output/path
	    -s, --source=SOURCE              Path to version controlled directory containing source md files
	    -o, --output=OUTPUT              Path to directory where output files are to be generated
	    -w, --watch                      Continuously watch the source directory for changes and generate output
	    -v, --verbose                    Print detailed information about files as they are processed
	    -t, --theme=PATH                 Path to directory containing theme files to use when generating html
	        --generate-theme=PATH        Generate a baseline theme that you can then modify to your needs
	        --benchmark                  Run a performance benchmark for the specified source
	    -h, --help                       Prints this help

### 1-to-1 mode

	➜  essays git:(main) ✗ bhook -s . -o /tmp/out -t /tmp/out/theme -w -v

Here's an example `.bhook.yml` [config file](https://gitlab.com/india-startups/wiki/-/blob/master/.bhook.yml) for a source directory.

### n-to-1 mode

This requires the destination directory to have a `.bhook.yml` config file set up that describes all the sources.

	➜  kaiwren.github.com git:(main) ✗ bhook -w -v

Here's an example `.bhook.yml` [config file](https://github.com/kaiwren/kaiwren.github.com/blob/master/.bhook.yml) for a destination directory
with multiple source directories.

## Development

1. `git clone --recursive git@gitlab.com:kaiwren/bhook.git`
2. After checking out the repo, run `bundle install` to install dependencies.
4. Then, run `bundle exec rake` to run the specs and sorbet typechecking. 

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/kaiwren/bhook.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'

RuboCop::RakeTask.new
RSpec::Core::RakeTask.new(:spec)

desc 'Run Sorbet Typechecker'
task :sorbet do
  sh('bundle exec srb tc')
end

basic_style_cops = %w[
  Layout/TrailingWhitespace
  Layout/SpaceInsideBlockBraces
  Style/StringLiterals
]

desc "Apply #{basic_style_cops.join(', ')}"
task :autocorrect_basic_style_issues do
  sh("bundle exec rubocop -a --only #{basic_style_cops.join(',')}")
end

namespace :ci do
  desc 'Tasks to run in Gitlab CI build stage'
  task build: %i[sorbet rubocop]

  desc 'Tasks to run in Gitlab CI spec stage'
  task spec: %i[spec]
end

task default: %i[autocorrect_basic_style_issues sorbet spec rubocop]

# typed: strict
# frozen_string_literal: true

require 'set'
require 'fileutils'
require 'pathname'
require 'erb'
require 'optparse'
require 'logger'
require 'uri'
require 'yaml'
require 'sorbet-runtime'
require 'git'
require 'kramdown'
require 'listen'

module Bhook
  extend T::Sig

  THEME_DIR_PATH = T.let(File.join(File.dirname(__FILE__), 'bhook', 'theme'), String)
  PAGE_TEMPLATE_PATH = T.let(File.join(THEME_DIR_PATH, 'page.erb'), String)
  AFTER_H1_TEMPLATE_PATH = T.let(File.join(THEME_DIR_PATH, '_after_h1.erb'), String)

  class Error < StandardError; end
end

require_relative 'bhook/version'
require_relative 'bhook/logger'
require_relative 'bhook/args_parser'
require_relative 'bhook/source_config'
require_relative 'bhook/theme_generator'
require_relative 'bhook/theme'
require_relative 'bhook/converter/html'
require_relative 'bhook/directory'
require_relative 'bhook/root_directory'
require_relative 'bhook/md_file'
require_relative 'bhook/workspace'
require_relative 'bhook/workspaces'

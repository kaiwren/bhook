# typed: strict
# frozen_string_literal: true

module Bhook
  #
  # Valid keys:
  # - website
  #   - link to place in the footer
  # - exclude
  #   - list of .md file paths relative to config files
  class SourceConfig
    extend T::Sig
    BHOOK_CONFIG_FILE = T.let('.bhook.yml', String)
    WEBSITE_KEY = T.let('website', String)
    EXCLUDE_KEY = T.let('exclude', String)

    sig { params(root_dir_path: Pathname, additional_options: T::Hash[String, String]).void }
    def initialize(root_dir_path, additional_options = {})
      config_file_path = root_dir_path.join(BHOOK_CONFIG_FILE)
      config = load_config(config_file_path).merge(additional_options)
      @root_dir_path = root_dir_path
      @website_url = T.let(config[WEBSITE_KEY], T.nilable(String))
      @excluded_files = T.let(parse_excluded_files(config[EXCLUDE_KEY] || []), T::Array[Pathname])
    end

    sig { params(src_file_path: Pathname, src_file_sha: T.nilable(String)).returns(T.nilable(String)) }
    def website_url_for(src_file_path, src_file_sha)
      return unless @website_url && src_file_sha

      relative_file_path = src_file_path.relative_path_from(@root_dir_path)
      File.join(@website_url, src_file_sha, relative_file_path)
    end

    sig { params(pathname: Pathname).returns(T::Boolean) }
    def excluded?(pathname)
      @excluded_files.include?(pathname)
    end

    private

    sig { params(excluded_files: T::Array[String]).returns(T::Array[Pathname]) }
    def parse_excluded_files(excluded_files)
      excluded_files.map { |file_path| @root_dir_path.join(file_path) if file_path }.compact
    end

    sig do
      params(config_file_path: Pathname).returns(T::Hash[String, T.untyped])
    end
    def load_config(config_file_path)
      if File.exist?(config_file_path)
        L.debug("Config found: #{config_file_path}")
        YAML.safe_load(File.read(config_file_path))
      else
        L.debug("Config not found: #{config_file_path}")
        {}
      end
    end
  end
end

# typed: strict
# frozen_string_literal: true

module Bhook
  class Workspaces
    extend T::Sig

    sig { void }
    def initialize
      @workspaces = T.let([], T::Array[Bhook::Workspace])
    end

    sig { params(workspace: Workspace).void }
    def mount(workspace)
      @workspaces << workspace
    end

    sig { void }
    def process!
      @workspaces.each(&:process!)
    end

    sig { void }
    def watch!
      @workspaces.each(&:watch!)
      Kernel.sleep
    end
  end
end

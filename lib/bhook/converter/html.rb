# typed: strict
# frozen_string_literal: true

module Bhook
  module Converter
    class Html < ::Kramdown::Converter::Html
      extend T::Sig

      sig { params(root: Kramdown::Element, options: T::Hash[Symbol, T.untyped]).void }
      def initialize(root, options)
        @options = T.let({}, T::Hash[Symbol, T.untyped])
        super(root, options)
      end

      sig { params(el: Kramdown::Element, indent: Integer).returns(String) }
      def convert_a(el, indent)
        href_path = el.attr['href']
        if valid_relative_md_link?(href_path)
          L.debug "Found link: #{href_path}"
          el.attr['href'].gsub!(/\.md/, '.html')
        end
        super(el, indent)
      end

      sig { params(el: Kramdown::Element, indent: Integer).returns(String) }
      def convert_header(el, indent)
        src_title = el.options[:raw_text]
        after_h1_html = @options[:after_h1_strategy].call(binding)

        level = output_header_level(el.options[:level])
        if level == 1
          @options[:h1_callback].call(src_title)
          "#{super(el, indent)}#{' ' * indent}#{after_h1_html}"
        else
          insert_anchor_into_header!(el)
          super(el, indent)
        end
      end

      private

      sig { params(el: Kramdown::Element).void }
      def insert_anchor_into_header!(el)
        el.attr['id'] = generate_id(el.options[:raw_text]) if @options[:auto_ids] && !el.attr['id']
        anchor = Kramdown::Element.new(:a, '', { 'href' => "##{el.attr['id']}", 'class' => 'header-anchor' })
        el.children << anchor
      end

      sig { params(href_path: String).returns(T::Boolean) }
      def valid_relative_md_link?(href_path)
        (href_path.end_with?('.md') || href_path.include?('.md#')) &&
          Pathname.new(href_path).relative?
      end
    end
  end
end

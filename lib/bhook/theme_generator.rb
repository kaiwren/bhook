# typed: strict
# frozen_string_literal: true

module Bhook
  class ThemeGenerator
    extend T::Sig

    sig { params(output_path: String).void }
    def initialize(output_path)
      @output_path = T.let(File.join(output_path, 'theme'), String)
      @template_files = T.let([Bhook::PAGE_TEMPLATE_PATH,
                               Bhook::AFTER_H1_TEMPLATE_PATH], T::Array[String])
    end

    sig { void }
    def generate!
      FileUtils.mkdir_p(@output_path)
      L.debug("mkdir: #{@output_path}")
      FileUtils.cp(@template_files, @output_path)
      L.debug("cp: #{@template_files.join(' ')} #{@output_path}")
    end
  end
end

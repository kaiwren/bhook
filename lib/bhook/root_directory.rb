# typed: strict
# frozen_string_literal: true

module Bhook
  class RootDirectory < Directory
    extend T::Sig

    sig { params(src_path: Pathname, out_path: Pathname).void }
    def initialize(src_path, out_path)
      super(src_path, out_path, Git.open(src_path), Bhook::SourceConfig.new(src_path))
    end

    private

    sig { params(_src_path: Pathname, out_path: Pathname).returns(Pathname) }
    def build_out_path(_src_path, out_path)
      out_path
    end
  end
end

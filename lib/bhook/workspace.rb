# typed: strict
# frozen_string_literal: true

module Bhook
  class Workspace
    extend T::Sig

    sig { params(src_path: String, out_path: String, theme_path: String).void }
    def initialize(src_path, out_path, theme_path)
      @src_path = T.let(Pathname.new(src_path).expand_path, Pathname)
      @out_path = T.let(Pathname.new(out_path).expand_path, Pathname)
      @theme = T.let(Theme.new(theme_path), Bhook::Theme)
    end

    sig { void }
    def process!
      root_dir.write!(@theme)
      L.info 'Done!'
    end

    sig { void }
    def watch!
      process!

      L.info "Watching: #{@src_path} for changes..."
      listener = Listen.to(@src_path.to_s, File.join(@src_path.to_s, '.git')) do |_modified, _added, _removed|
        L.info 'Detected changes...'
        process!
      end
      listener.start
    end

    private

    sig { returns(Bhook::Directory) }
    def root_dir
      RootDirectory.new(@src_path, @out_path)
    end
  end
end

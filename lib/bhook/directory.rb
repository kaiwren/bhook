# typed: strict
# frozen_string_literal: true

module Bhook
  class Directory
    extend T::Sig

    GIT_DIR = '.git'
    MD_EXT = '.md'

    sig { params(src_path: Pathname, out_path: Pathname).returns(Bhook::RootDirectory) }
    def self.new_root_directory(src_path, out_path)
      RootDirectory.new(src_path, out_path)
    end

    sig { params(src_path: Pathname, out_path: Pathname, git: Git::Base, config: Bhook::SourceConfig).void }
    def initialize(src_path, out_path, git, config)
      @src_path = src_path
      @out_path = T.let(build_out_path(src_path, out_path), Pathname)
      @git = git
      @config = config
      @sub_dirs = T.let([], T::Array[Directory])
      @md_files = T.let([], T::Array[MdFile])
      build_next_level_nodes
    end

    sig { params(theme: Theme).void }
    def write!(theme)
      FileUtils.mkdir_p(@out_path)
      L.debug("mkdir: #{@out_path}")
      @sub_dirs.each { |dir| dir.write!(theme) }
      @md_files.map do |file|
        Thread.new { file.write!(theme) }
      end.each(&:join)
    end

    sig { returns(T::Array[MdFile]) }
    def all_md_files
      @md_files + @sub_dirs.map(&:all_md_files).flatten
    end

    sig { returns(String) }
    def to_s
      @src_path.to_s
    end

    private

    sig { params(src_path: Pathname, out_path: Pathname).returns(Pathname) }
    def build_out_path(src_path, out_path)
      out_path.join(src_path.basename)
    end

    sig { void }
    def build_next_level_nodes
      children = @src_path.children
      children.delete(@src_path.join(GIT_DIR))
      build_nodes(children)
    end

    sig { params(children: T::Array[Pathname]).void }
    def build_nodes(children)
      file_threads = []
      children.each do |child_path|
        if child_path.directory?
          @sub_dirs << Directory.new(child_path, @out_path, @git, @config)
        elsif child_path.extname == MD_EXT && !@config.excluded?(child_path)
          file_threads << Thread.new { MdFile.new(child_path, @out_path, @git, @config) }
        end
      end
      file_threads.each { |thread| @md_files << thread.value }
    end
  end
end

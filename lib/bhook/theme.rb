# typed: strict
# frozen_string_literal: true

module Bhook
  class Theme
    extend T::Sig

    sig { params(theme_path: String).void }
    def initialize(theme_path)
      @page_template = T.let(File.read(File.join(theme_path, 'page.erb')), String)
      @after_h1_template = T.let(File.read(File.join(theme_path, '_after_h1.erb')), String)
      strategy = ->(binding_instance) { ERB.new(@after_h1_template, trim_mode: '-').result(binding_instance) }
      @after_h1_strategy = T.let(strategy, T.proc.params(binding_instance: Binding).returns(String))
    end

    sig do
      params(md: String, src_file_sha: T.nilable(String),
             src_file_date: T.nilable(String), file_url: T.nilable(String)).returns(String)
    end
    def render_page(md, src_file_sha, src_file_date, file_url)
      src_title = T.let('', String)

      doc = Kramdown::Document.new(md)
      output, warnings = Converter::Html.convert(doc.root, doc.options.merge(
                                                             after_h1_strategy: @after_h1_strategy,
                                                             h1_callback: ->(str) { src_title = str }
                                                           ))
      ERB.new(@page_template, trim_mode: '-').result(binding)
    end
  end
end

# typed: true
# frozen_string_literal: true

module Bhook
  LOG_FORMATTER = proc { |_severity, _datetime, _progname, msg|
    "#{Thread.current.object_id} #{msg}\n"
  }
  L = Logger.new($stdout, formatter: LOG_FORMATTER)
end

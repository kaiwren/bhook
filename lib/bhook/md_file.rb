# typed: strict
# frozen_string_literal: true

module Bhook
  class MdFile
    extend T::Sig

    PAGE_TEMPLATE = T.let(File.read(Bhook::PAGE_TEMPLATE_PATH), String)
    AFTER_H1_TEMPLATE = T.let(File.read(Bhook::AFTER_H1_TEMPLATE_PATH), String)

    sig { returns(Pathname) }
    attr_reader :src_file_path, :out_file_path

    sig { returns(T.nilable(String)) }
    attr_reader :src_file_date, :src_file_sha, :src_file_url

    sig { returns(String) }
    attr_reader :md

    sig { params(src_file_path: Pathname, out_path: Pathname, git: Git::Base, config: Bhook::SourceConfig).void }
    def initialize(src_file_path, out_path, git, config)
      L.debug "Reading: #{src_file_path}"
      @md = T.let(File.read(src_file_path), String)
      @src_file_path = src_file_path
      @out_path = out_path
      @git = git
      @config = config
      @out_file_path = T.let(@out_path.join(out_file_name), Pathname)
      initialize_file_details
    end

    sig { params(theme: Bhook::Theme).void }
    def write!(theme)
      L.debug "Processing: #{@src_file_sha || 'unversioned'} #{@src_file_path}"
      rendered_page = theme.render_page(@md, @src_file_sha, @src_file_date, @src_file_url)

      L.debug "Writing: #{@src_file_sha} #{out_file_path}"
      File.write(@out_file_path, rendered_page)
    end

    private

    sig { returns(Pathname) }
    def out_file_name
      @src_file_path.basename.sub(/\.md$/, '.html')
    end

    sig { returns(T.nilable(String)) }
    def file_url
      @config.website_url_for(@src_file_path, @src_file_sha)
    end

    sig { returns(T::Array[String]) }
    def load_git_file_metadata
      @git.lib.send(:command, 'log',
                    '-n 1',
                    '--pretty=format:%ad|%H',
                    '--date=short',
                    '--',
                    @src_file_path).split('|')
    end

    sig { void }
    def initialize_file_details
      file_date, file_sha = load_git_file_metadata
      @src_file_date = T.let(file_date, T.nilable(String))
      @src_file_sha = T.let(file_sha, T.nilable(String))
      @src_file_url = T.let(file_url, T.nilable(String))
    end
  end
end

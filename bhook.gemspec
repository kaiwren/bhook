# frozen_string_literal: true

require_relative 'lib/bhook/version'

Gem::Specification.new do |spec|
  spec.name = 'bhook'
  spec.version = Bhook::VERSION
  spec.authors = ['Sidu Ponnappa']
  spec.email = ['ckponnappa@gmail.com']

  spec.summary = 'Git markdown wiki to HTML'
  spec.description = 'Convert your Git based markdown wiki repo to a static HTML website'
  spec.homepage = 'https://gitlab.com/kaiwren/bhook'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.7.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'bin'
  spec.executables = spec.files.grep(%r{\Abin/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  spec.add_runtime_dependency 'git', '~> 1.10'
  spec.add_runtime_dependency 'kramdown', '~> 2.3'
  spec.add_runtime_dependency 'listen', '~> 3.7'
  spec.add_runtime_dependency 'sorbet-runtime', '~> 0.5'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rspec', '~> 3.1'
  spec.add_development_dependency 'rubocop', '~> 1.0'
  spec.add_development_dependency 'rubocop-rake', '~> 0.6'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.8'
  spec.add_development_dependency 'simplecov', '~> 0.21'
  spec.add_development_dependency 'simplecov-cobertura', '~> 2.1.0'
  spec.add_development_dependency 'sorbet', '~> 0.5'

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.metadata['rubygems_mfa_required'] = 'true'
end
